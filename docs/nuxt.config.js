
export default {
  mode: 'universal',
  server: {
    port: 3020, // default: 3000
    host: '0.0.0.0', // default: localhost,
    timing: true
  },
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'mobile-web-app-capable', content:'yes'},
      { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=5, minimal-ui' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss',
  ],
  // tailwindcss: {
  //   configPath: '~/tailwind.config.js',
  //   cssPath: '~/assets/css/tailwind.css',
  //   purgeCSSInDev: true
  // },
  // purgeCSS: {
  //   paths: [
  //     '.nuxt/exodia/templates/**/*.vue',
  //   ],
  // },
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    // ['@eka_kurnia1/exodia', {
    //   namespace: 'exodia',
    //   initialValue: 6, 
    // }],
    ['../../lib/module.js', { 
      namespace: 'exodia',
      initialValue: 6, 
      // debug: true
    }]
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },
  /*
  ** Build configuration
  */
  build: {
    // analyze: true,
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
      config.node = {
          fs: 'empty'
      }

      config.module.rules.push({
        enforce: 'pre',
        test: /\.txt$/,
        loader: 'raw-loader',
        exclude: /(node_modules)/
      });
    }
  }
}
