export default {
  menu: [
    // {
    //   label: 'Pendahuluan',
    //   path: '/',
    //   icon: '',
    //   children: [
    //     {
    //       label: 'Pertanyaan',
    //       path: '/introduction/faq',
    //     },
    //     {
    //       label: 'Roadmap',
    //       path: '/introduction/roadmap',
    //     }
    //   ]
    // },
    // {
    //   label: 'Memulai',
    //   path: '/getting-started/quick-start',
    //   icon: '',
    //   children: [
    //     {
    //       label: 'Mulai Cepat',
    //       path: '/getting-started/quick-start',
    //     }
    //   ]
    // },
    {
      label: 'Komponen UI',
      path: '/components/alerts',
      icon: '',
      expand: true,
      children: [
        {
          label: 'Alert',
          path: '/components/alert',
        },
        {
          label: 'BottomSheet',
          path: '/components/bottom-sheet',
        },
        {
          label: 'Button',
          path: '/components/button',
          badge: {
            label: 'need improve',
            color: 'white',
            background: 'brown'
          }
        },
        {
          label: 'Card',
          path: '/components/card',
        },
        // {
        //   label: 'ListView',
        //   path: '/components/list',
        // },
        {
          label: 'Modal',
          path: '/components/modal',
        },
        {
          label: 'Confirm',
          path: '/components/confirm',
          // badge: {
          //   label: 'todo',
          //   color: 'white',
          //   background: 'green'
          // }
        },
        // {
        //   label: 'NavBar',
        //   path: '/components/navbar',
        // },
        {
          label: 'Neumorphism',
          path: '/components/neumorphism',
          badge: {
            label: 'research',
            color: 'white',
            background: 'purple'
          }
        },
        {
          label: 'Form',
          path: '/components/radio',
          badge: {
            label: 'todo',
            color: 'white',
            background: 'green'
          },
          children: [
            {
              label: 'DateTimePicker',
              path: '/components/date-time-picker',
              badge: {
                label: 'need improve',
                color: 'white',
                background: 'brown'
              }
            },
            {
              label: 'Checkbox',
              path: '/components/checkbox',
              badge: {
                label: 'todo',
                color: 'white',
                background: 'green'
              }
            },
            {
              label: 'Radio',
              path: '/components/radio',
              // badge: {
              //   label: 'todo',
              //   color: 'white',
              //   background: 'green'
              // }
            },
            {
              label: 'Switch',
              path: '/components/switch',
              // badge: {
              //   label: 'todo',
              //   color: 'white',
              //   background: 'green'
              // }
            },
          ]
        },
        {
          label: 'ProgressBar',
          path: '/components/progress-bar',
        },
        {
          label: 'Select',
          path: '/components/select',
        },
        {
          label: 'Timeline',
          path: '/components/timeline',
        },
        {
          label: 'Tab',
          path: '/components/tab',
          // badge: {
          //   label: 'todo',
          //   color: 'white',
          //   background: 'green'
          // }
        },
        {
          label: 'Stepper',
          path: '/components/stepper',
          badge: {
            label: 'todo',
            color: 'white',
            background: 'green'
          }
        },
        // {
        //   label: 'Text',
        //   path: '/components/text',
        // },
        {
          label: 'TextField',
          path: '/components/textfield',
          badge: {
            label: 'need improve',
            color: 'white',
            background: 'brown'
          }
        },
        {
          label: 'Toast',
          path: '/components/toast',
        },
        {
          label: 'Test',
          path: '/components/test',
          badge: {
            label: 'research',
            color: 'white',
            background: 'purple'
          }
        },
      ]
    },
    {
      label: 'Alat',
      path: '/tools/playground',
      expand: true,
      children: [
        {
          label: 'Playground',
          path: '/tools/playground',
        }
      ]
    }


  ]
}