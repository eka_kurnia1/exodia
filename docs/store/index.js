
export const state = () => ({
  treeMenu: [],
  menu: []
})

export const mutations = {
  treeMenu(state, data) {
    state.treeMenu = data
  },
  menu(state, data) {
    state.menu = data
  }
}

export const getters = {
  treeMenu: state => {
    const list = [
      ...state.treeMenu
    ] 
    
    list.sort((a, b) => {
      const valA = a['name'].toUpperCase()
      const valB = b['name'].toUpperCase()
      let comparison = 0;
      if (valA > valB) {
        comparison = 1;
      } else if (valA < valB) {
        comparison = -1;
      }
      return comparison;
    })

    return  list
  },
  menu: state => state.menu
}

export const actions = {
  async nuxtServerInit({ state, commit, dispatch }, context) {
    // dispatch('readDir')
    dispatch('fetchMenu')
  },
  readDir({ commit }) {
    var temp = []
    const { readdirSync } = require('fs');
    var components = readdirSync('./pages/components')
    components.forEach(component => {
      if(component.substr(component.length - 4, component.length) === '.vue') {
        var componentName = component.replace('.vue', '')
        temp.push({
          name: componentName,
          path: `/components/${componentName}`,
          component: 'page-'+componentName
        })
      }
    });

    commit('treeMenu', temp)
  },
  fetchMenu({ commit }) {
    var menu = require('@/static/menu').default
    commit('menu', menu['menu'])
  }
}

