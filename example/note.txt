server {
        listen 80;

        server_name scratch.yukbisnis.com;

        access_log /var/log/nginx/scratch.yukbisnis.com-access.log main;
        error_log /var/log/nginx/scratch.yukbisnis.com-error.log;

        location ~ ^\/(.*)$ {
               proxy_pass http://127.0.0.1:3020;
               proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection 'upgrade';
                proxy_set_header Host $host;
                proxy_cache_bypass $http_upgrade;
        }

        location = / {
                proxy_pass http://127.0.0.1:3020;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection 'upgrade';
                proxy_set_header Host $host;
                proxy_cache_bypass $http_upgrade;
        }

        location = /registration {
                proxy_pass http://127.0.0.1:3002;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection 'upgrade';
                proxy_set_header Host $host;
                proxy_cache_bypass $http_upgrade;
        }

        location = /landing {
                proxy_pass http://127.0.0.1:3002;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection 'upgrade';
                proxy_set_header Host $host;
                proxy_cache_bypass $http_upgrade;
        }
}