
// middleware/index.js
import Middleware from '../../middleware'
const options = JSON.parse(`<%= JSON.stringify(options) %>`)
const { namespace } = options
Middleware[namespace] = context => {
  const { route, store, app } = context
  // simply console logging here to demonstrate access to app context.
  console.log('Exodia middleware route', route.path)
  console.log('Exodia middleware store', store.state[namespace].count)
  console.log('Exodia middleware app', app[`$${namespace}`].value())
}