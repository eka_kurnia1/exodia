// module.js
const { resolve, join } = require('path')
const { readdirSync, lstat } = require('fs')

export default function(moduleOptions) {
  // get all options for the module
  const options = {
    ...moduleOptions,
    ...this.options.customCounter,
  }

  // expose the namespace / set a default
  if (!options.namespace) options.namespace = 'exodia'
  const { namespace } = options

  // console.log('options', this.options)
  this.options['purgeCSS'] = {
    paths: [
      `.nuxt/${namespace}/templates/**/*.vue`,
    ],
  }

  // add all of the initial plugins
  const pluginsToSync = [
    'utils/bus.js',
    'plugins/toast.js',
    'plugins/confirm.js',
    'plugins/component.js',
    'templates/index.js',
    'store/index.js',
    'plugins/index.js',
    'debug.js',
    'middleware/index.js'
  ]

  for (const pathString of pluginsToSync) {
    this.addPlugin({
      src: resolve(__dirname, pathString),
      fileName: join(namespace, pathString),
      options
    })
  }

  var isGlobal = options['global'] === undefined ? true : options['global'] 
  
  // sync all of the files and folders to revelant places in the nuxt build dir (.nuxt/)
  const foldersToSync = [
    'plugins/helpers', 
    'store/modules', 
    'templates/components'
  ]

  // if(isGlobal) {
  //   foldersToSync.push('templates/components')
  // }

  for (const pathString of foldersToSync) {
    const path = resolve(__dirname, pathString)
    
    for (const file of readdirSync(path)) {
      lstat(join(path, file), (err, stats) => {

        if(err) {
          return console.log(err); //Handle error
        }

        if(stats.isDirectory()) {
          for (const subFile of readdirSync(join(path, file))) {
            const path2 = resolve(__dirname, join(path, file))
            
            // console.log('src', resolve(path2, subFile))
            // console.log('filename', join(namespace, pathString, file))

            this.addTemplate({
              src: resolve(path2, subFile),
              fileName: join(namespace, join(pathString, file), subFile),
              options
            })
          }
        } else {
          this.addTemplate({
            src: resolve(path, file),
            fileName: join(namespace, pathString, file),
            options
          })
        }
      })
    }
  }
}
module.exports.meta = require('../package.json')