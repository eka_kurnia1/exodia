import Component from '../templates/components/Confirm/index.vue'
import eventBus from '../utils/bus.js';

const Api = (Vue, globalOptions = {}) => {
  return {
    show(options) {
      let message;
      if (typeof options === 'string') message = options;

      const defaultOptions = {
        message
      };

      const propsData = Object.assign({}, defaultOptions, globalOptions, options);

      return new (Vue.extend(Component))({
        el: document.createElement('div'),
        propsData
      })
    },
    close() {
      eventBus.$emit('confirm.clear')
    },
    default(message, options = {}) {
      return this.show(Object.assign({}, {
        message,
        type: 'default'
      }, options))
    }
  }
};

export default Api;