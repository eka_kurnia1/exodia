import Component from '../templates/components/Toast/index.vue'
import eventBus from '../utils/bus.js';

const Api = (Vue, globalOptions = {}) => {
  return {
    show(options) {
      let message;
      if (typeof options === 'string') message = options;

      const defaultOptions = {
        message
      };

      const propsData = Object.assign({}, defaultOptions, globalOptions, options);

      return new (Vue.extend(Component))({
        el: document.createElement('div'),
        propsData
      })
    },
    clear() {
      eventBus.$emit('toast.clear')
    },
    success(message, options = {}) {
      return this.show(Object.assign({}, {
        message,
        type: 'success'
      }, options))
    },
    error(message, options = {}) {
      return this.show(Object.assign({}, {
        message,
        type: 'error'
      }, options))
    },
    info(message, options = {}) {
      return this.show(Object.assign({}, {
        message,
        type: 'info'
      }, options))
    },
    warning(message, options = {}) {
      return this.show(Object.assign({}, {
        message,
        type: 'warning'
      }, options))
    },
    default(message, options = {}) {
      return this.show(Object.assign({}, {
        message,
        type: 'default'
      }, options))
    }
  }
};

export default Api;